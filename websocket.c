/*
 * Sapphire backend
 *
 * Copyright (C) 2018 Alyssa Rosenzweig
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 *
 */

#include <stdio.h>
#include <purple.h>

#include <gio/gio.h>
#include <gio/gunixsocketaddress.h>
#include <glib/gstdio.h>

#include "core.h"
#include "websocket.h"
#include "push.h"

#define WS_PORT 7070

/* This module is responsible for networking via UNIX sockets, translated to
 * WebSockets at the proxy level. */

/* The connection to the proxy */
Connection *proxy = NULL;

void
sapphire_send_raw_packet(const char *frame)
{
	if (!proxy) {
		fprintf(stderr, "send: No proxy\n");
		return;
	}

	if (!g_socket_connection_is_connected(proxy->connection)) {
		fprintf(stderr, "send: Tried to send %s to closed connection, ignoring\n", frame);
		return;
	}

	GError *gerror = NULL;
	GOutputStream *ostream = g_io_stream_get_output_stream(G_IO_STREAM(proxy->connection));

	g_output_stream_write_all(ostream, frame, strlen(frame), NULL, NULL, &gerror);

	if (gerror) {
		fprintf(stderr, "send: Got gerror %s!\n", gerror->message);
		gerror = NULL;
		return;
	}

	char end = '\n';
	g_output_stream_write(ostream, &end, 1, NULL, &gerror);

	if (gerror) {
		fprintf(stderr, "send: Got gerror v2!\n");
	}
}

/* Sends the packet to any connection, or save it for the next connection */

GSList *queued_messages = NULL;

/* XXX copypasted from proxy.c */

static void sapphire_got_line(GObject *source_object, GAsyncResult *res, gpointer user_data);

static void
sapphire_read_line()
{
	g_data_input_stream_read_line_async(proxy->distream, G_PRIORITY_DEFAULT, NULL, sapphire_got_line, proxy);
}

static void
sapphire_got_line(GObject *source_object,
                        GAsyncResult *res,
                        gpointer user_data)
{
	Connection *conn = (Connection *) user_data;

	GError *err = NULL;
	gsize len;
	char *data = g_data_input_stream_read_line_finish_utf8(G_DATA_INPUT_STREAM(source_object), res, &len, &err);

	if (err || !data) {
		/* Borp, error -- disconnect */

		/* Free the connection */
		g_hash_table_remove_all(conn->subscribed_ids);

		/* Splice the socket out of the authenticated list, so we no longer
		 * attempt to broadcast to it */
		proxy = NULL;

		g_free(conn);

		/* Disconnect accounts if needed */
		sapphire_enable_accounts_by_connections();

		return;
	}

	/* The message should be interpreted as JSON, decode that here */

	JsonParser *parser = json_parser_new();

	if (!json_parser_load_from_data(parser, data, -1, NULL)) {
		fprintf(stderr, "Error parsing response: %s\n", data);
		fprintf(stderr, "^ Couldn't do it\n");
		goto refresh;
	}

	JsonNode *root = json_parser_get_root(parser);

	if (root == NULL) {
		fprintf(stderr, "websocket: NULL root, ignoring\n");
		goto refresh;
	}

	/* Delegate off */

	JsonObject *obj = json_node_get_object(root);
	sapphire_process_message(conn, obj);

refresh:
	g_free(data);
	g_object_unref(parser);
	sapphire_read_line(conn);
}


gboolean
incoming_callback  (GSocketService *service,
                    GSocketConnection *connection,
                    GObject *source_object,
                    gpointer user_data)
{
	g_print("Received Connection from client!\n");

	/* Allocate a connection object for us and fill it in */
	Connection *conn = g_new0(Connection, 1);

	conn->is_authenticated = FALSE;

	/* Save the connection.
	 * IMPORTANT: Reference counting is necessary to keep the connection
	 * alive. No idea why this isn't documented anywhere, xxx
	 */

	conn->connection = g_object_ref(connection);

	if (proxy) {
		/* Warning: we're leaking the old proxy instance TODO */
	}

	proxy = conn;

	/* Subscribe to incoming data */

	GInputStream *istream = g_io_stream_get_input_stream (G_IO_STREAM (conn->connection));
	conn->distream = g_data_input_stream_new(istream);
	sapphire_read_line(conn);

	return FALSE;
}

void
sapphire_init_websocket(void)
{
	const gchar *filename = "./sockpuppet";
	g_unlink(filename);

	GSocketService *service = g_socket_service_new();
	GSocketAddress *addr = g_unix_socket_address_new (filename);

	g_socket_listener_add_address(G_SOCKET_LISTENER(service), addr, G_SOCKET_TYPE_STREAM, G_SOCKET_PROTOCOL_DEFAULT, NULL, NULL, NULL);
	g_signal_connect (service, "incoming", G_CALLBACK (incoming_callback), NULL);
}
