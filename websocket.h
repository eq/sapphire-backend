/*
 * Sapphire backend
 *
 * Copyright (C) 2018 Alyssa Rosenzweig
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 *
 */

#ifndef __WEBSOCKET_H_
#define __WEBSOCKET_H_

#include <libsoup/soup.h>
#include <purple.h>

/* Represents a connection to a given client. Minimal state should be kept
 * here, since connections are device-specific, not for the client as a whole.
 * Essentially, just enough for the websocket metadata and a little potpourrie */

typedef struct Connection {
	/* The reference counted connection itself */
	GSocketConnection *connection;
	GDataInputStream *distream;

	/* Has this connection authenticated yet? */
	gboolean is_authenticated;

	/* Subscribed conversation IDs, for which */
	GHashTable *subscribed_ids;
} Connection;

void sapphire_init_websocket(void);
void sapphire_send_raw_packet(const char *packet);

void sapphire_add_buddy_icon(const gchar *name, PurpleBuddyIcon *icon);
void sapphire_add_stored_image(const gchar *name, PurpleStoredImage *img);

#endif
